#ifndef IMAGE_TRANSFORMER_UTILS
#define IMAGE_TRANSFORMER_UTILS

#include "./image.h"
#include <stdint.h>
#include <stdio.h>

void printLn(char *message);
uint8_t calc_padding(uint64_t byte_size);
#endif
