#include "../include/image.h"

struct image create_image(uint64_t width, uint64_t height)
{
  struct image img = {0};
  img.width = width;
  img.height = height;
  img.data = malloc(width * height * sizeof(struct pixel));
  return img;
}
